const mongoose =require('mongoose');
const Schema = mongoose.Schema;

const user = new Schema({
  
    firstName:{
        type:String,
        required:true,
        trim:true
    },
    lastName:{
        type:String,
        required:true,
        trim:true
    }
    // date:Date.now()
})
const users = mongoose.model('student',user);
module.exports =users;
