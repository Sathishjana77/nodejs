const express = require('express');
const mongoose=require('mongoose');
const router = require('./routes/router');
const passport = require('passport');
const passport_auth0= require('passport-auth0');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');
const app =express();


app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json);

PORT=3000;
app.listen(PORT,()=>{
    console.log('port running on http://localhost:'+PORT);
})