const express =require('express');
const bodyParser=require('body-Parser');
const app=express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());


const college= require('./routers/college');
const dept= require('./routers/dept');
const hods= require('./routers/hods');
const facility= require('./routers/facility');
const student= require('./routers/student');

app.use('/college',college);
app.use('/dept',dept);
app.use('/hods',hods);
app.use('/facility',facility);
app.use('/student',student);





app.listen(2000,function(){
    console.log("listening on 2000");
})