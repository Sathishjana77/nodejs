const express =require('express');
const router =express.Router();
const youtube = require('../models/yotube');

router.post('/',async(req,res,next)=>{
    try {
        const a = await youtube.create(req.body)
        // res.send(a);
        res.send({
            status:200,
            message:"user created",
            data:a
        })
        // status(200).json(a);
        console.log("user created");
    } catch (error) {
        console.log('error');
        status(501).json({"message":"page not res fail to create"})
        
    }
})
router.get('/',async(req,res,next)=>{
try {
    const a = await youtube.find({});
    res.json(a);
    // res.send({
    //     status:200,
    //     data:a
    // })
} catch (error) {
    console.log('error')
    status(501).json({"message":"fail to fetch data"})
}

})

// to get one user details am just gin=ving url for ref = localhost:3000/youtube/user/just give user id on here it will return that user exact what u are requesting

router.get('/user/:id',async(req,res,next)=>{
    try {
        const a =await youtube.findById(req.params.id);
        // status(200).json(a);
        res.send({
            status:200,
            data:a
        })
        
    } catch (error) {
        // console.log('error')
        // status(501).json({"message":"fail to fetch data"})
        res.send({
          status:501,
          message:"fail to fetch sry for that"
        })
    }
    
    })

// put method 

// input method before u are trying to run please check the schema  first based on schema u need to give the input body on postman

router.put('/user/:id',async(req,res,next)=>{
         
        try {
            const a = await youtube.findById(req.params.id);
             a.set(req.body);   
            const r = await a.save();
            // status(200).json(r)
            res.send({
                status:200,
                data:r
            })
            
        } catch (error) {
            res.send({
                status:501,
                message:"internal error"
            })
        }

})

// delete method

router.delete('/user/:id',async(req,res,next)=>{
    try {
        const a = await youtube.deleteOne({_id:req.params.id})
        res.send({
            status:200,
            data:a,
            message:"user removed successfully"
        })
        
    } catch (error) {

        res.send({
            status:500,
            message:"error"
        })
        
    }
})

module.exports=router;