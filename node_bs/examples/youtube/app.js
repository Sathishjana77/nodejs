const mongoose =require('mongoose');
const config = require('config');
const bodyParser =require('body-parser');
const express =require('express');
const routes =require('./routes/youtube');
const db =require('./test/db')

const app = express();

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/youtube',routes)


PORT =config.get('port');
app.listen(PORT,()=>{
    console.log('port ruunig on' + PORT);
})