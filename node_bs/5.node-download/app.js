const express = require('express');
const app = express();
const fs = require('fs');
app.get('/download', function (request, response) {
    var filePath = "./public/";
    var fileName = request.query.fn;
    if (fileName.trimLeft!= "" && fileName.trimLeft()!=undefined) {

       fs.access((filePath + fileName), function () {


            response.download(filePath + fileName);
        })
    }
    else {
        response.send("missing file");
    }
});


app.listen(2000, function () {
    console.log("ok");
})
